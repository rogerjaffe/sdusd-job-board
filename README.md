CURRENT DEPLOYMENT
on PHHS server in /job-board folder

URLs:
Spreadsheet for data: https://docs.google.com/spreadsheets/d/1sTpEzQ6s2uWeTIBkYeHMd_Tb2s6xdHiIIHOsaTH5HxE/edit#gid=0
Webpage: https://storage.googleapis.com/job-board/index.html

DEPLOYMENT TO Google Cloud
Deploy GC bucket for `rjaffe-sup@sandi.net`, project `sdusd-job-board`, bucket `job-board`

Create bucket: gsutil mb -l us-west1 gs://job-board/

Deploy (copy) files: gsutil cp -r ./build/* gs://job-board/
Remember to build first!!

Make public: gsutil acl ch -u AllUsers:R gs://job-board/

Or drag and drop the /build folder contents on the GC console dashboard

Work Permit home page (as of 2020-08-30 https://sandiegounified.org/cms/one.aspx?portalId=27732478&pageId=28619295)

Modifications to this page made 2020-08-30 because SDUSD went to a new website vendor
and all CSS / JS links broke.  These were re-configured so this page matches
the styling of the district site.

Note that the resulting web pages are CSS formatted to be consistent with other SDUSD webpages
