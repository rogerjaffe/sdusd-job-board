import React, { Component } from 'react';
import GetSheetDone from 'get-sheet-done';
import moment from 'moment'
import './JobBoard.css'
import pkg from "../package.json";

const R = require('ramda')

const DATA_URL = 'https://docs.google.com/spreadsheets/d/1sTpEzQ6s2uWeTIBkYeHMd_Tb2s6xdHiIIHOsaTH5HxE/edit#gid=0'
const ID = 5

class JobBoard extends Component {

  constructor() {
    super()
    this.state = {
      processing: true,
      sheetName: '',
      updated: null,
      data: []
    }
  }

  componentDidMount() {
    let _this = this
    let id = R.split('/', DATA_URL)[ID]
    let data = GetSheetDone.labeledCols(id)
      .then(data => {
        // Filter out items that are "removed" or are past the deadline
        const today = moment()
        let list = R.filter(item => (item.openuntil.toLowerCase() === 'open' || moment(item.openuntil).isSameOrAfter(today)) && item.removed.length === 0 , data.data)
        list = R.sort((a,b) => {
          if (a.openuntil.toLowerCase() === 'open' && b.openuntil.toLowerCase() !== 'open') {
            return 1
          } else if (a.openuntil.toLowerCase() !== 'open' && b.openuntil.toLowerCase() === 'open') {
            return -1
          } else if (a.openuntil.toLowerCase() === 'open' && b.openuntil.toLowerCase() === 'open') {
            return 0
          } else if (moment(a.openuntil).isBefore(moment(b.openuntil))) {
            return -1
          } else {
            return 1
          }
        }, list)
        _this.setState({
          processing: false,
          sheetName: data.title,
          updated: moment(data.updated).format("MMM D YYYY"),
          data: list
        })
        console.log(data)
      })
  }

  render() {
    if (this.state.processing) {
      return (<h4 className="please-standby wrap">Retrieving job board information...</h4>)
    } else {
      return (
        <div>
          <table className="job-board wrap">
            <thead>
            <tr>
              <th>Deadline</th>
              <th>Location</th>
              <th>Description</th>
              <th>Contact / Phone</th>
              <th>Email</th>
            </tr>
            </thead>
            <tbody>
            {
              this.state.data.map((item, idx) =>
                <tr key={idx}>
                  <td width="10%">{item.openuntil}</td>
                  <td>{item.locationaddress}</td>
                  <td>{item.description}</td>
                  <td width="15%">{item.contactname}<br/>{item.phone}</td>
                  <td><a href={'mailto:' + item.email}>{item.email}</a></td>
                </tr>
              )
            }

            </tbody>
            <tfoot>
              <tr>
                <td colSpan={4}>
                  <span>Maintained by Dawn Marino</span><br/>
                  <a href="dmarino@sandi.net">dmarino@sandi.net</a> 858-503-1750<br/>
                  <span>Version {pkg.version}</span>
                </td>
              </tr>
            </tfoot>
          </table>
        </div>
      );
    }
  }
}

export default JobBoard;
