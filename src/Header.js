import React, { Component } from 'react';

class Header extends Component {
  render() {
    return (
      <div className="page_header" id="page_header">
        <a name="main-content" id="main-content"></a>
        <div className="wrap">
          <h1>Job Board</h1>
          <div className="breadcrumbs">
            <h2 className="element-invisible">You are here</h2>
            <div className="breadcrumb"><a href="/">Home</a> » <a href="/academics" title="">Academics</a> » <a
              href="https://www.sandiegounified.org/parent-student-resources" title="">Parent &amp; Student Resources</a></div>
          </div>
        </div>
      </div>
    );
  }
}

export default Header;
