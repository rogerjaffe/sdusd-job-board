import React, { Component } from 'react';
import JobBoard from './JobBoard'

class App extends Component {
  render() {
    return (
      <div className="App">
        <JobBoard></JobBoard>
      </div>
    );
  }
}

export default App;
